# AWS Infrastructure Setup with Terraform

This project uses Terraform to set up a basic AWS infrastructure for a web application. The infrastructure includes an EC2 instance running an Ubuntu server, an S3 bucket for storing static assets, a security group that allows HTTP (port 80) and SSH (port 22) access, and an IAM role with permissions to access the S3 bucket.

## Directory Structure


```
.
├── README.md
├── dev
│   ├── main.tf
│   ├── outputs.tf
│   ├── terraform.tfstate
│   ├── terraform.tfstate.backup
│   ├── terraform.tfvars
│   └── variables.tf
└── modules
    ├── ec2
    │   ├── main.tf
    │   ├── outputs.tf
    │   └── variables.tf
    ├── iam
    │   ├── main.tf
    │   ├── outputs.tf
    │   └── variables.tf
    ├── s3
    │   ├── main.tf
    │   ├── outputs.tf
    │   └── variables.tf
    └── security_group
        ├── main.tf
        ├── outputs.tf
        └── variables.tf
```


## Prerequisites

- AWS CLI installed and configured with appropriate credentials.
- Terraform installed on your local machine.
- Valid AWS Access Key ID and Secret Access Key.

## Setup Instructions

1. **Initialize Terraform**:
   Navigate to the `dev` directory and initialize Terraform.

```
   sh
   cd dev
   terraform init
```

   

2. **Plan the Infrastructure**:
   Generate an execution plan to see what Terraform will do.

```
   sh
   terraform plan
```

   

3. **Apply the Configuration**:
   Apply the Terraform configuration to create the resources.

```
   sh
   terraform apply
```

   

   Type `yes` when prompted to confirm.

4. **Destroy the Infrastructure (if needed)**:
   To destroy the resources created by Terraform, run:

```
   sh
   terraform destroy
```

   

   Type `yes` when prompted to confirm.

## Explanation of the Script

### Provider Configuration

In `dev/main.tf`, we define the AWS provider and specify the region:

```
provider "aws" {
  region = var.aws_region
}
```



### EC2 Instance Module

The EC2 instance module (`modules/ec2/main.tf`) sets up an Ubuntu server:


```
resource "aws_instance" "web" {
  ami                    = var.ami
  instance_type          = var.instance_type
  iam_instance_profile   = var.iam_instance_profile
  vpc_security_group_ids = var.security_group_ids

  user_data = <<-EOF
              #!/bin/bash
              apt-get update -y
              apt-get install -y nginx
              systemctl start nginx
              systemctl enable nginx
              EOF

  tags = {
    Name = "WebServerInstance"
  }
}
```



### S3 Bucket Module

The S3 bucket module (`modules/s3/main.tf`) creates an S3 bucket with versioning enabled:


```
resource "aws_s3_bucket" "static_assets" {
  bucket = var.bucket_name

  tags = {
    Name = "StaticAssetsBucket"
  }
}

resource "aws_s3_bucket_versioning" "versioning" {
  bucket = aws_s3_bucket.static_assets.bucket
  versioning_configuration {
    status = "Enabled"
  }
}
```


### IAM Role Module

The IAM role module (`modules/iam/main.tf`) creates an IAM role with permissions to access the S3 bucket:


```
resource "aws_iam_role" "s3_access" {
  name = "s3_access_role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Effect = "Allow"
        Principal = {
          Service = "ec2.amazonaws.com"
        }
        Action = "sts:AssumeRole"
      }
    ]
  })
}

resource "aws_iam_role_policy" "s3_access_policy" {
  name   = "s3_access_policy"
  role   = aws_iam_role.s3_access.id

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Effect = "Allow"
        Action = [
          "s3:GetObject",
          "s3:PutObject",
          "s3:ListBucket"
        ]
        Resource = [
          "${var.s3_bucket_arn}",
          "${var.s3_bucket_arn}/*"
        ]
      }
    ]
  })
}

resource "aws_iam_instance_profile" "s3_access" {
  name = "s3_access_instance_profile"
  role = aws_iam_role.s3_access.name
}
```


### Security Group Module

The security group module (`modules/security_group/main.tf`) defines rules to allow HTTP and SSH access:


```
resource "aws_security_group" "web_sg" {
  name        = "web_sg"
  description = "Allow HTTP and SSH inbound traffic"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "WebSecurityGroup"
  }
}
```



## Assumptions

- **AMI**: The AMI ID provided in `terraform.tfvars` is for Ubuntu 20.04 LTS and is valid in the specified region.
- **Permissions**: The AWS credentials used have sufficient permissions to create the resources defined in the script.
- **Region**: The infrastructure will be created in the region specified in the `aws_region` variable.

## Conclusion

This Terraform script provides a modular and reusable setup for a basic web application infrastructure on AWS. It includes best practices for security and infrastructure as code, making it easy to manage and extend.
