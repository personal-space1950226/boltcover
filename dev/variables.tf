variable "aws_region" {
  description = "The AWS region to deploy to"
  default     = "us-east-1"
}

variable "instance_type" {
  description = "Type of EC2 instance"
  default     = "t2.micro"
}

variable "ami" {
  description = "AMI ID for the EC2 instance"
  default     = "ami-00d96722e56ad1cbc"
}

variable "bucket_name" {
  description = "Name of the S3 bucket"
  default     = "boltcover-bucket"
}
