provider "aws" {
  region = var.aws_region
}

module "s3" {
  source = "../modules/s3"
  bucket_name = var.bucket_name
}

module "iam" {
  source = "../modules/iam"
  s3_bucket_arn = module.s3.bucket_arn
}

module "security_group" {
  source = "../modules/security_group"
}

module "ec2" {
  source = "../modules/ec2"
  instance_type     = var.instance_type
  ami               = var.ami
  iam_instance_profile = module.iam.instance_profile_name
  security_group_ids = [module.security_group.security_group_id]
}
