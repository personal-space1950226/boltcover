variable "instance_type" {
  description = "Type of EC2 instance"
}

variable "ami" {
  description = "AMI ID for the EC2 instance"
}

variable "iam_instance_profile" {
  description = "IAM instance profile name"
}

variable "security_group_ids" {
  description = "List of security group IDs"
}
