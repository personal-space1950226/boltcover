output "bucket_id" {
  value = aws_s3_bucket.static_assets.id
}

output "bucket_arn" {
  value = aws_s3_bucket.static_assets.arn
}
