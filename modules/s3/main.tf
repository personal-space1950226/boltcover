resource "aws_s3_bucket" "static_assets" {
  bucket = var.bucket_name

  tags = {
    Name = "StaticAssetsBucket"
  }
}

resource "aws_s3_bucket_versioning" "versioning" {
  bucket = aws_s3_bucket.static_assets.bucket
  versioning_configuration {
    status = "Enabled"
  }
}
